/*
 * IWindow.h
 *
 *  Created on: 8 abr. 2020
 *      Author: rober
 */

#ifndef INCLUDE_RENGINE_CORE_WINDOW_IWINDOW_H_
#define INCLUDE_RENGINE_CORE_WINDOW_IWINDOW_H_
#include <string>
class IWindow {
public:
	IWindow(){};
	virtual ~IWindow(){};
	virtual void CloseWindow()= 0;
	virtual void CleanWindow()= 0;
	virtual float GetDeltaTime() = 0;
	virtual bool CheckWindowContext()= 0;
	virtual std::string ProcessInput()=0;
};



#endif /* INCLUDE_RENGINE_CORE_WINDOW_IWINDOW_H_ */
