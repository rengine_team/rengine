/*
 * WindowGLFW.h
 *
 *  Created on: 8 abr. 2020
 *      Author: rober
 */

#ifndef RENGINE_CORE_WINDOW_GLFWINDOW_WINDOWGLFW_H_
#define RENGINE_CORE_WINDOW_GLFWINDOW_WINDOWGLFW_H_

#include <ReNgine/Core/Window/IWindow.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <ReNgine/GlobalConst.h>
namespace ReNgine {

class WindowGLFW : public IWindow {
public:
	WindowGLFW();
	virtual ~WindowGLFW();
	void CloseWindow();
	void CleanWindow();
	float GetDeltaTime();
	bool CheckWindowContext();
	std::string ProcessInput();
private:
	GLFWwindow * window;
float lastFrame = 0;
};

} /* namespace ReNgine */

#endif /* RENGINE_CORE_WINDOW_GLFWINDOW_WINDOWGLFW_H_ */
