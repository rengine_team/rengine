
#ifndef ISHADER_H
#define ISHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <glm/glm.hpp>
#include <glad/glad.h>

class IShader
{
public:
	IShader(){};
    virtual ~IShader(){};
    //IShader(const std::string vertexPath, const std::string fragmentPath, const char* geometryPath = nullptr){};
    virtual void use()=0;
    virtual void setBool(const std::string &name, bool value) const=0;
    virtual void setInt(const std::string &name, int value) const=0;
    virtual void setFloat(const std::string &name, float value) const=0;
    virtual void setVec2(const std::string &name, const glm::vec2 &value) const=0;
    virtual void setVec3(const std::string &name, const glm::vec3 &value) const=0;
    virtual void setVec4(const std::string &name, const glm::vec4 &value) const=0;
    virtual void setMat2(const std::string &name, const glm::mat2 &mat) const=0;
    virtual void setMat3(const std::string &name, const glm::mat3 &mat) const=0;
    virtual void setMat4(const std::string &name, const glm::mat4 &mat) const=0;
};
#endif
