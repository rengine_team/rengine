/*
 * IRenderAddon.h
 *
 *  Created on: 10 abr. 2020
 *      Author: rober
 */

#ifndef INCLUDE_RENGINE_RENDERING_IRENDERADDON_H_
#define INCLUDE_RENGINE_RENDERING_IRENDERADDON_H_

class IRenderAddOn {
public:
	IRenderAddOn(){};
	virtual ~IRenderAddOn(){};
	virtual void Display(){};
};



#endif /* INCLUDE_RENGINE_RENDERING_IRENDERADDON_H_ */
