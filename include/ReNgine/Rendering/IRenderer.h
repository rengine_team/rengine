/*
 * IRenderer.h
 *
 *  Created on: 8 abr. 2020
 *      Author: rober
 */

#ifndef INCLUDE_RENGINE_RENDERING_IRENDERER_H_
#define INCLUDE_RENGINE_RENDERING_IRENDERER_H_
#include <ReNgine/Core/Window/IWindow.h>
#include <ReNgine/Rendering/OpenGL/Model/Model.h>
#include <ReNgine/Rendering/OpenGL/Shader/ShaderGL.h>
#include <vector>
#include <string>
class IRenderer {
public:
	glm::mat4 view;
	glm::mat4 projection;
	IRenderer(){};
	virtual ~IRenderer(){};
	void virtual Draw(Model model, ShaderGL shader, glm::vec3 position)=0;
	void virtual ClearScreen()=0;
	void virtual UpdateSystemConfigurations(float xoffset, float yoffset, std::string key, float deltaTime, bool mouseEvent)=0;
};



#endif /* INCLUDE_RENGINE_RENDERING_IRENDERER_H_ */
