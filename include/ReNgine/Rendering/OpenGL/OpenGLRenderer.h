/*
 * OpenGLRenderer.h
 *
 *  Created on: 8 abr. 2020
 *      Author: rober
 */

#ifndef RENGINE_RENDERING_OPENGL_OPENGLRENDERER_H_
#define RENGINE_RENDERING_OPENGL_OPENGLRENDERER_H_
#include <ReNgine/Rendering/IRenderer.h>
#include <ReNgine/Rendering/OpenGL/Camera/CameraGL.h>
namespace ReNgine {

class OpenGLRenderer : public IRenderer {
public:
	OpenGLRenderer();
	OpenGLRenderer(std::vector<std::string> initConfig);
	void Draw(Model model, ShaderGL shader, glm::vec3 position);
	virtual ~OpenGLRenderer();
	void ClearScreen();
	void UpdateSystemConfigurations(float xoffset, float yoffset, std::string key, float deltaTime,bool mouseEvent);
private:
	void UpdateShaderConfigurations(ShaderGL shader);

	CameraGL camera;
	unsigned int lastShader;
};

} /* namespace ReNgine */

#endif /* RENGINE_RENDERING_OPENGL_OPENGLRENDERER_H_ */
