/*
 * GLShadowMap.h
 *
 *  Created on: 8 abr. 2020
 *      Author: rober
 */

#ifndef RENGINE_RENDERING_OPENGL_GLSHADOWMAP_H_
#define RENGINE_RENDERING_OPENGL_GLSHADOWMAP_H_

namespace ReNgine {

class GLShadowMap {
public:
	GLShadowMap();
	virtual ~GLShadowMap();
};

} /* namespace ReNgine */

#endif /* RENGINE_RENDERING_OPENGL_GLSHADOWMAP_H_ */
