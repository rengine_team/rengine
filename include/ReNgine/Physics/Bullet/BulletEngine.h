/*
 * BulletEngine.h
 *
 *  Created on: 7 abr. 2020
 *      Author: rober
 */

#ifndef PHYSICS_BULLETENGINE_BULLETENGINE_H_
#define PHYSICS_BULLETENGINE_BULLETENGINE_H_
#include <bullet/btBulletDynamicsCommon.h>
#include <ReNgine/Physics/IPhysicsEngine.h>
#include <ReNgine/Physics/Bullet/BulletDebugDrawerOpenGL.h>
class BulletEngine: public IPhysicsEngine {
public:
	BulletEngine();
	virtual ~BulletEngine();
	unsigned int AddRigidBodyBox(glm::vec3 origin, float i_mass,glm::vec3 size);
	void StepSimulation(float dt);
	glm::vec3 GetPhysicsPosition(unsigned int index);
	void DrawPhysics(glm::mat4 ViewMatrix,glm::mat4 ProjectionMatrix);
	unsigned int AddRigidBodySphere(glm::vec3 origin, float i_mass,float radio);
private:
	BulletDebugDrawer_OpenGL drawer;
	btDiscreteDynamicsWorld *dynamicsWorld;
	btAlignedObjectArray<btCollisionShape*> collisionShapes;
};

#endif /* PHYSICS_BULLETENGINE_BULLETENGINE_H_ */
