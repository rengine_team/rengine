/*
 * ReN_IPhysicsEngine.h
 *
 *  Created on: 7 abr. 2020
 *      Author: rober
 */

#ifndef PHYSICS_IPHYSICSENGINE_H_
#define PHYSICS_IPHYSICSENGINE_H_

#include <glm/glm.hpp>

class IPhysicsEngine {
public:
	IPhysicsEngine(){};
	virtual ~IPhysicsEngine(){};
	virtual unsigned int AddRigidBodyBox(glm::vec3 origin, float i_mass,glm::vec3 size)=0;
	virtual void StepSimulation(float dt)=0;
	virtual glm::vec3 GetPhysicsPosition(unsigned int index) = 0;
	virtual void DrawPhysics(glm::mat4 ViewMatrix,glm::mat4 ProjectionMatrix) = 0;
	virtual unsigned int AddRigidBodySphere(glm::vec3 origin, float i_mass,float radio)=0;
};



#endif /* PHYSICS_IPHYSICSENGINE_H_ */
