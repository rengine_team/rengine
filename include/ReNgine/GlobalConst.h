/*
 * GlobalConst.h
 *
 *  Created on: 10 abr. 2020
 *      Author: rober
 */

#ifndef RENGINE_GLOBALCONST_H_
#define RENGINE_GLOBALCONST_H_


extern float lastX;
extern float lastY;
extern bool firstMouse;
extern float xoffset;
extern float yoffset;
extern bool mouseEvent;

#endif /* RENGINE_GLOBALCONST_H_ */
