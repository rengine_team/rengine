/*
 * ReNgine.h
 *
 *  Created on: 9 abr. 2020
 *      Author: rober
 */

#ifndef RENGINE_H_
#define RENGINE_H_
#include <ReNgine/Rendering/IRenderer.h>
#include <ReNgine/Physics/IPhysicsEngine.h>
#include <ReNgine/Physics/Bullet/BulletEngine.h>
#include <ReNgine/Rendering/OpenGL/OpenGLRenderer.h>
#include <ReNgine/Core/Window/IWindow.h>
#include <ReNgine/Core/Window/WindowGLFW.h>
#include <string>
#include <ReNgine/GlobalConst.h>
namespace ReNgine {

class ReNgine {
public:
	ReNgine(std::string windowContext, std::string renderSystem);
	virtual ~ReNgine();
	void ProcessInput(float dt);
	void DrawPhysics();
	IWindow* window;
	IRenderer* render;
	IPhysicsEngine* physicsEngine;
};

} /* namespace ReNgine */

#endif /* RENGINE_H_ */
