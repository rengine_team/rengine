/*
 * BulletEngine.cpp
 *
 *  Created on: 7 abr. 2020
 *      Author: rober
 */

#include <ReNgine/Physics/Bullet/BulletEngine.h>

BulletEngine::BulletEngine() {
	btDefaultCollisionConfiguration *collisionConfiguration =
			new btDefaultCollisionConfiguration();
	btCollisionDispatcher *dispatcher = new btCollisionDispatcher(
			collisionConfiguration);
	btBroadphaseInterface *overlappingPairCache = new btDbvtBroadphase();
	btSequentialImpulseConstraintSolver *solver =
			new btSequentialImpulseConstraintSolver;
	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher,
			overlappingPairCache, solver, collisionConfiguration);
	dynamicsWorld->setGravity(btVector3(0, -10, 0));

	dynamicsWorld->setDebugDrawer(&drawer);
}
void BulletEngine::StepSimulation(float dt) {

	dynamicsWorld->stepSimulation(1/60.0f, 10);
}
glm::vec3 BulletEngine::GetPhysicsPosition(unsigned int index) {
	btCollisionObject *obj = dynamicsWorld->getCollisionObjectArray()[index];
	btRigidBody *body = btRigidBody::upcast(obj);
	btTransform trans;
	if (body && body->getMotionState()) {
		body->getMotionState()->getWorldTransform(trans);
	} else {
		trans = obj->getWorldTransform();
	}
	float x = float(trans.getOrigin().getX());
	float y = float(trans.getOrigin().getY());
	float z = float(trans.getOrigin().getZ());
	printf("worldposobject%d=%f,%f,%f\n", index, x,y,z);
	return glm::vec3(x,y,z);
}
void BulletEngine::DrawPhysics(glm::mat4 ViewMatrix,
		glm::mat4 ProjectionMatrix) {
	drawer.SetMatrices(ViewMatrix, ProjectionMatrix);
	dynamicsWorld->debugDrawWorld();
}
unsigned int BulletEngine::AddRigidBodyBox(glm::vec3 origin, float i_mass,glm::vec3 size) {
	btCollisionShape *groundShape = new btBoxShape(
			btVector3(btScalar(size.x), btScalar(size.y), btScalar(size.z)));

	collisionShapes.push_back(groundShape);

	btTransform groundTransform;
	groundTransform.setIdentity();
	groundTransform.setOrigin(btVector3(origin.x, origin.y, origin.z));

	btScalar mass(i_mass);

	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
		groundShape->calculateLocalInertia(mass, localInertia);

	btDefaultMotionState *myMotionState = new btDefaultMotionState(groundTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
	rbInfo.m_restitution = 0.75;
	btRigidBody *body = new btRigidBody(rbInfo);

	dynamicsWorld->addRigidBody(body);
	return dynamicsWorld->getNumCollisionObjects()-1;
}
unsigned int BulletEngine::AddRigidBodySphere(glm::vec3 origin, float i_mass,float radio) {
	btCollisionShape *groundShape = new btSphereShape(radio);

	collisionShapes.push_back(groundShape);

	btTransform groundTransform;
	groundTransform.setIdentity();
	groundTransform.setOrigin(btVector3(origin.x, origin.y, origin.z));

	btScalar mass(i_mass);

	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
		groundShape->calculateLocalInertia(mass, localInertia);

	btDefaultMotionState *myMotionState = new btDefaultMotionState(groundTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
	rbInfo.m_restitution = 1;
	//rbInfo.
	btRigidBody *body = new btRigidBody(rbInfo);

	dynamicsWorld->addRigidBody(body);
	return dynamicsWorld->getNumCollisionObjects()-1;
}
BulletEngine::~BulletEngine() {
	// TODO Auto-generated destructor stub
}

