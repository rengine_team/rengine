/*
 * WindowGLFW.cpp
 *
 *  Created on: 8 abr. 2020
 *      Author: rober
 */

#include <ReNgine/Core/Window/WindowGLFW.h>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
float lastX = (float)800 / 2.0;
float lastY = (float)600 / 2.0;
bool firstMouse = true;
float xoffset = 0;
float yoffset = 0;
bool mouseEvent = false;
namespace ReNgine {

WindowGLFW::WindowGLFW() {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 2);

	window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		//LOG::LogConsole("Failed to create GLFW window");

	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		//LOG::LogConsole("Failed to initialize GLAD");
		glfwTerminate();
	}
}

WindowGLFW::~WindowGLFW() {
	// TODO Auto-generated destructor stub
}
float WindowGLFW::GetDeltaTime() {
	float currentFrame = glfwGetTime();
	float deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;
	return deltaTime;
}
std::string WindowGLFW::ProcessInput(){
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		return "W";
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		return "S";
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		return "A";
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		return "D";
	return "";
}
} /* namespace ReNgine */

void ReNgine::WindowGLFW::CloseWindow() {
	glfwTerminate();
}

void ReNgine::WindowGLFW::CleanWindow() {
	glfwSwapBuffers(window);
	glfwPollEvents();
}

bool ReNgine::WindowGLFW::CheckWindowContext() {
	return glfwWindowShouldClose(window);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    xoffset = xpos - lastX;
    yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;
    mouseEvent = true;
    //game.camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    //game.camera.ProcessMouseScroll(yoffset);
}
