/*
 * OpenGLRenderer.cpp
 *
 *  Created on: 8 abr. 2020
 *      Author: rober
 */

#include <ReNgine/Rendering/OpenGL/OpenGLRenderer.h>

namespace ReNgine {
OpenGLRenderer::~OpenGLRenderer() {
	// TODO Auto-generated destructor stub

}
OpenGLRenderer::OpenGLRenderer() {
	camera = CameraGL(glm::vec3(0.0f, 4.0f, 18.5f));
	lastShader = 0;
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	//glEnable(GL_CULL_FACE);
}
OpenGLRenderer::OpenGLRenderer(std::vector<std::string> initConfig) {
	camera = CameraGL(glm::vec3(0.0f, 4.0f, 18.5f));
	lastShader = 0;
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	//glEnable(GL_CULL_FACE);
}

void OpenGLRenderer::Draw(Model model, ShaderGL shader, glm::vec3 position) {
	//if (shader.ID != lastShader) {
		shader.use();
		/*cout << "Cambio de shader" << endl;
		lastShader = shader.ID;
	}*/
	glm::mat4 model_mtx = glm::mat4(1.0f);
	model_mtx[3] = glm::vec4(position,1);
	shader.setMat4("model",model_mtx);
	UpdateShaderConfigurations(shader);
	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;
	unsigned int normalNr = 1;
	unsigned int heightNr = 1;
	for (unsigned int j = 0; j < model.meshes.size(); j++) {
		for (unsigned int i = 0; i < model.meshes[j].textures.size(); i++) {
			glActiveTexture(GL_TEXTURE0 + i + 1); // active proper texture unit before binding
			// retrieve texture number (the N in diffuse_textureN)
			string number;
			string name = model.meshes[j].textures[i].type;
			if (name == "texture_diffuse")
				number = std::to_string(diffuseNr++);
			else if (name == "texture_specular")
				number = std::to_string(specularNr++); // transfer unsigned int to stream
			else if (name == "texture_normal")
				number = std::to_string(normalNr++); // transfer unsigned int to stream
			else if (name == "texture_height")
				number = std::to_string(heightNr++); // transfer unsigned int to stream

			// now set the sampler to the correct texture unit
			glUniform1i(
			glGetUniformLocation(shader.ID, (name + number).c_str()), i + 1);
			// and finally bind the texture
			glBindTexture(GL_TEXTURE_2D, model.meshes[j].textures[i].id);
		}

		// draw mesh
		glBindVertexArray(model.meshes[j].VAO);
		glDrawElements(GL_TRIANGLES, model.meshes[j].indices.size(),
		GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);

		// always good practice to set everything back to defaults once configured.
		glActiveTexture(GL_TEXTURE0);
	}
}
void OpenGLRenderer::ClearScreen() {
	glClearColor(1.2f, 0.4f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
void OpenGLRenderer::UpdateSystemConfigurations(float xoffset, float yoffset,std::string key, float deltaTime,bool mouseEvent) {
	if(mouseEvent){
	camera.ProcessMouseMovement(xoffset, yoffset);
	mouseEvent = false;
	}
	if (key == "W")
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (key == "S")
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (key == "A")
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (key == "D")
		camera.ProcessKeyboard(RIGHT, deltaTime);
}
void OpenGLRenderer::UpdateShaderConfigurations(ShaderGL shader) {

	projection = glm::perspective(glm::radians(camera.Zoom),
			(float) 800 / (float) 600, 0.1f, 100.0f);
	view = camera.GetViewMatrix();
	shader.setMat4("projection", projection);
	shader.setMat4("view", view);

	shader.setVec3("objectColor", glm::vec3(1.0f));
	shader.setVec3("lightColor", glm::vec3(1.0f, 1, 1));
	shader.setVec3("lightPos", glm::vec3(0, 8.0f, 0));
}
} /* namespace ReNgine */
