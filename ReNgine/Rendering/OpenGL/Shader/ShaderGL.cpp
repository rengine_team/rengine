

#include <ReNgine/Rendering/OpenGL/Shader/ShaderGL.h>

ShaderGL::ShaderGL()
{
}
ShaderGL::~ShaderGL()
{
}
// ------------------------------------------------------------------------
ShaderGL::ShaderGL(const std::string vertexPath, const std::string fragmentPath, const char* geometryPath)
{
	// 1. retrieve the vertex/fragment source code from filePath
	std::string vertexCode;
	std::string fragmentCode;
	std::string geometryCode;
	std::ifstream vShaderGLFile;
	std::ifstream fShaderGLFile;
	std::ifstream gShaderGLFile;
	// ensure ifstream objects can throw exceptions:
	vShaderGLFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
	fShaderGLFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
	gShaderGLFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		// open files
		vShaderGLFile.open(vertexPath);
		fShaderGLFile.open(fragmentPath);
		std::stringstream vShaderGLStream, fShaderGLStream;
		// read file's buffer contents into streams
		vShaderGLStream << vShaderGLFile.rdbuf();
		fShaderGLStream << fShaderGLFile.rdbuf();
		// close file handlers
		vShaderGLFile.close();
		fShaderGLFile.close();
		// convert stream into string
		vertexCode = vShaderGLStream.str();
		fragmentCode = fShaderGLStream.str();
		// if geometry ShaderGL path is present, also load a geometry ShaderGL
		if(geometryPath != nullptr)
		{
			gShaderGLFile.open(geometryPath);
			std::stringstream gShaderGLStream;
			gShaderGLStream << gShaderGLFile.rdbuf();
			gShaderGLFile.close();
			geometryCode = gShaderGLStream.str();
		}
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "ERROR::ShaderGL::FILE_NOT_SUCCESFULLY_READ" << std::endl;
		std::cout << vertexPath << std::endl;
		std::cout << e.what() << std::endl;
		std::cout << fragmentPath << std::endl;
	}
	const char* vShaderGLCode = vertexCode.c_str();
	const char * fShaderGLCode = fragmentCode.c_str();
	// 2. compile ShaderGLs
	unsigned int vertex, fragment;
	int success;
	char infoLog[512];
	// vertex ShaderGL
	vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex, 1, &vShaderGLCode, NULL);
	glCompileShader(vertex);
	checkCompileErrors(vertex, "VERTEX");
	// fragment ShaderGL
	fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment, 1, &fShaderGLCode, NULL);
	glCompileShader(fragment);
	checkCompileErrors(fragment, "FRAGMENT");
	// if geometry ShaderGL is given, compile geometry ShaderGL
	unsigned int geometry;
	if(geometryPath != nullptr)
	{
		const char * gShaderGLCode = geometryCode.c_str();
		geometry = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(geometry, 1, &gShaderGLCode, NULL);
		glCompileShader(geometry);
		checkCompileErrors(geometry, "GEOMETRY");
	}
	// ShaderGL Program
	ID = glCreateProgram();
	glAttachShader(ID, vertex);
	glAttachShader(ID, fragment);
	if(geometryPath != nullptr)
		glAttachShader(ID, geometry);
	glLinkProgram(ID);
	checkCompileErrors(ID, "PROGRAM");
	// delete the ShaderGLs as they're linked into our program now and no longer necessery
	glDeleteShader(vertex);
	glDeleteShader(fragment);
	if(geometryPath != nullptr)
		glDeleteShader(geometry);

}
// activate the ShaderGL
// ------------------------------------------------------------------------
void ShaderGL::use()
{
	glUseProgram(ID);
}
// utility uniform functions
// ------------------------------------------------------------------------
void ShaderGL::setBool(const std::string &name, bool value) const
{
	glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value);
}
// ------------------------------------------------------------------------
void ShaderGL::setInt(const std::string &name, int value) const
{
	glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
}
// ------------------------------------------------------------------------
void ShaderGL::setFloat(const std::string &name, float value) const
{
	glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}
// ------------------------------------------------------------------------
void ShaderGL::setVec2(const std::string &name, const glm::vec2 &value) const
{
	glUniform2fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}
// ------------------------------------------------------------------------
void ShaderGL::setVec3(const std::string &name, const glm::vec3 &value) const
{
	glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}
void ShaderGL::setVec4(const std::string &name, const glm::vec4 &value) const
{
	glUniform4fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}
void ShaderGL::setMat2(const std::string &name, const glm::mat2 &mat) const
{
	glUniformMatrix2fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
// ------------------------------------------------------------------------
void ShaderGL::setMat3(const std::string &name, const glm::mat3 &mat) const
{
	glUniformMatrix3fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
// ------------------------------------------------------------------------
void ShaderGL::setMat4(const std::string &name, const glm::mat4 &mat) const
{
	glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}


// utility function for checking ShaderGL compilation/linking errors.
// ------------------------------------------------------------------------
void ShaderGL::checkCompileErrors(GLuint ShaderGL, std::string type)
{
	GLint success;
	GLchar infoLog[1024];
	if(type != "PROGRAM")
	{
		glGetShaderiv(ShaderGL, GL_COMPILE_STATUS, &success);
		if(!success)
		{
			glGetShaderInfoLog(ShaderGL, 1024, NULL, infoLog);
			std::cout << "ERROR::ShaderGL_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
		}
	}
	else
	{
		glGetProgramiv(ShaderGL, GL_LINK_STATUS, &success);
		if(!success)
		{
			glGetProgramInfoLog(ShaderGL, 1024, NULL, infoLog);
			std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
		}
	}
}

