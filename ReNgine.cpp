/*
 * ReNgine.cpp
 *
 *  Created on: 9 abr. 2020
 *      Author: rober
 */

#include <ReNgine/ReNgine.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
namespace ReNgine {

ReNgine::~ReNgine() {
// TODO Auto-generated destructor stub
}

ReNgine::ReNgine(std::string windowContext, std::string renderSystem) {
	if (windowContext == "GLFW") {
		window = new WindowGLFW();
	}
	std::vector<std::string> initConfig;
	if (renderSystem == "OPENGL") {
		render = new OpenGLRenderer(initConfig);
	}
	physicsEngine = new BulletEngine();
}
void ReNgine::ProcessInput(float dt){
	std::string result = window->ProcessInput();
	render->UpdateSystemConfigurations(xoffset,yoffset,result,dt,mouseEvent);
	mouseEvent = false;
}
void ReNgine::DrawPhysics(){
	physicsEngine->DrawPhysics(render->view,render->projection);
}
}
